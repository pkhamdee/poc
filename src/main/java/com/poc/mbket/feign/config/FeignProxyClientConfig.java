package com.poc.mbket.feign.config;

import com.poc.mbket.config.properties.FeignProxyProperties;
import feign.Client;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.util.StringUtils;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@AllArgsConstructor
public class FeignProxyClientConfig {
    private final FeignProxyProperties feignProxyProperties;

    @ConditionalOnProperty(value = "mbket.feign.proxy.enabled", havingValue = "true")
    @Bean
    public Client client() {
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(feignProxyProperties.getHost(), feignProxyProperties.getPort()));
        return new DynamicProxyClient(null,
                null,
                proxy,
                transformNonProxyHosts(feignProxyProperties.getNonProxyHosts()));
    }

    private List<String> transformNonProxyHosts(String nonProxyHosts) {
        if (StringUtils.hasText(nonProxyHosts)) {
            return Arrays.asList(feignProxyProperties.getNonProxyHosts().split(FeignProxyProperties.HOST_SEPARATOR));
        }
        return Collections.emptyList();
    }

    static class DynamicProxyClient extends Client.Proxied {
        private final List<String> nonProxyHosts;

        public DynamicProxyClient(SSLSocketFactory sslContextFactory, HostnameVerifier hostnameVerifier, Proxy proxy, List<String> nonProxyHosts) {
            super(sslContextFactory, hostnameVerifier, proxy);
            this.nonProxyHosts = nonProxyHosts;
        }

        @Override
        public HttpURLConnection getConnection(URL url) throws IOException {
            if (nonProxyHosts.stream().anyMatch(host -> host.equals(url.getHost()))) {
                return (HttpURLConnection) url.openConnection();
            }
            return super.getConnection(url);
        }
    }
}
