package com.poc.mbket.entity;

import lombok.Data;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.TimeToLive;
import org.springframework.data.redis.core.index.Indexed;

@Data
@RedisHash(value = "poc-entity")
public class AppEntity {
    @Indexed
    private Integer id;
    private Boolean flag;
    private String text;
    @TimeToLive
    private Long ttl;
}
