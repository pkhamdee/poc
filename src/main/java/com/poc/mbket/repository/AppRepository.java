package com.poc.mbket.repository;

import com.poc.mbket.entity.AppEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppRepository extends CrudRepository<AppEntity, Long> {
}
