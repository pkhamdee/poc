package com.poc.mbket.controller.interceptor;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogRequestIdInterceptor extends HandlerInterceptorAdapter {
    private static final String REQUEST_ID = "request_id";
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        ThreadContext.put(REQUEST_ID, request.getHeader(REQUEST_ID));
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        ThreadContext.remove(REQUEST_ID);
    }
}
