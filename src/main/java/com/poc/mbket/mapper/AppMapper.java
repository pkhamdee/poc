package com.poc.mbket.mapper;

import com.poc.mbket.dto.AppRequest;
import com.poc.mbket.dto.AppResponse;
import com.poc.mbket.entity.AppEntity;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface AppMapper {

    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "text", source = "text")
    @Mapping(target = "flag", source = "flag")
    @Mapping(target = "ttl", constant = "900L")
    AppEntity toEntity(AppRequest request);

    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "respText", source = "text")
    @Mapping(target = "respFlag", source = "flag")
    AppResponse fromEntity(AppEntity entity);
}
