package com.poc.mbket.mapper;

import com.poc.mbket.dto.AppResponse;
import com.poc.mbket.entity.AppEntity;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class AppMapperTest {
    private final AppMapper MAPPER = Mappers.getMapper(AppMapper.class);

    @Test
    public void testToEntity() {
        AppEntity entity = new AppEntity();
        entity.setText("txt");
        entity.setFlag(false);
        AppResponse response = MAPPER.fromEntity(entity);

        assertEquals("txt", response.getRespText());
        assertFalse(response.getRespFlag());
    }
}
